<?php

abstract class Person {
    abstract function isMale();
    abstract function getCode();
    // ...
}

class Male extends Person {
    public function isMale()
    {
        return true;
    }

    public function getCode()
    {
        return 'M';
    }
}

class Female extends Person {
    public function isMale()
    {
        return false;
    }

    public function getCode()
    {
        return 'F';
    }
}