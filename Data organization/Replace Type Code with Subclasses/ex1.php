<?php

class Employee {
    private $type;

    const ENGINEER = 0;
    const SALESMAN = 1;
    const MANAGER = 2;

    public function __construct($type)
    {
        $this->type = $type;
    }
}