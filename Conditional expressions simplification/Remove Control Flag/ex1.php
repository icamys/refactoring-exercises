<?php

/**
 * Conditions:
 * - People names should stay hard coded
 */

function sendAlert() {/* ... */}

function checkSecurity(array $people) {
    /** @var bool $found */
    $found = false;

    for ($i = 0; $i < count($people); $i++) {
        if (! $found) {
            if ($people[$i] === 'Don') {
                sendAlert();
                $found = true;
            }
            if ($people[$i] === 'John') {
                sendAlert();
                $found = true;
            }
        }
    }
}