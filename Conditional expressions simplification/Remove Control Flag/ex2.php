<?php

/**
 * Conditions:
 * - People names should stay hard coded
 */

function sendAlert() {/* ... */}
function someLaterCode($name) {/* ... */}

function checkSecurity(array $people) {
    /** @var string $found */
    $found = '';

    for ($i = 0; $i < count($people); $i++) {
        if (strlen($found) === 0) {
            if ($people[$i] === 'Don') {
                sendAlert();
                $found = 'Don';
            }
            if ($people[$i] === 'John') {
                sendAlert();
                $found = 'John';
            }
        }
    }

    someLaterCode($found);
}