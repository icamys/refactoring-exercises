<?php

class Employee  {
    // ...

    public function payAmount() {
        switch ($this->getType()) {
            case EmployeeType::ENGINEER:
                return $this->monthlySalary;
            case EmployeeType::SALESMAN:
                return $this->monthlySalary + $this->commission;
            case EmployeeType::MANAGER:
                return $this->monthlySalary + $this->bonus;
            default:
                throw new RuntimeException('Incorrect Employee');
        }
    }

    public function getType() {
        return $this->type->getTypeCode();
    }

    /** @var EmployeeType */
    private $type;

    private $monthlySalary;
    private $commission;
    private $bonus;
}

abstract class EmployeeType {
    // ...

    const ENGINEER = 0;
    const SALESMAN = 1;
    const MANAGER = 2;

    abstract public function getTypeCode();
}

class Engineer extends EmployeeType {
    // ...

    public function getTypeCode()
    {
        return EmployeeType::ENGINEER;
    }
}

class Salesman extends EmployeeType {
    // ...

    public function getTypeCode()
    {
        return EmployeeType::SALESMAN;
    }
}

class Manager extends EmployeeType {
    // ...

    public function getTypeCode()
    {
        return EmployeeType::MANAGER;
    }
}