<?php

class Employee {
    private $isDead;
    private $isSeparated;
    private $isRetired;

    public function deadAmount() {/* ... */}
    public function separatedAmount() {/* ... */}
    public function retiredAmount() {/* ... */}
    public function normalPayAmount() {/* ... */}

    // ...

    public function getPaymentCount() {
        $result = null;
        if ($this->isDead) $result = $this->deadAmount();
        else {
            if ($this->isSeparated) $result = $this->separatedAmount();
            else {
                if ($this->isRetired) $result = $this->retiredAmount();
                else $result = $this->normalPayAmount();
            }
        }
        return $result;
    }

    // ...
}