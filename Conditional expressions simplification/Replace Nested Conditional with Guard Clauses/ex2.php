<?php

// Tip: First reverse conditions, then make it guard clauses

class System {
    const ADJ_FACTOR = 0.5;

    private $capital;
    private $intRate;
    private $duration;
    private $income;

    // ...

    public function getAdjustedCapital() {
        $result = 0;
        if ($this->capital > 0) {
            if ($this->intRate > 0 && $this->duration > 0) {
                $result = ($this->income / $this->duration) * self::ADJ_FACTOR;
            }
        }
        return $result;
    }
}